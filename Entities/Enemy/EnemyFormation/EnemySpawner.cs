﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyPrefab;
	public GameObject enemyPrefab2;

	public GameObject boss01;

	public float width = 10.0f;
	public float height = 5.0f;
	public float speed = 20.0f;
	public float spawnDelay = 0.5f;
	public string gameLevelLoaded;

	private bool movingRight = true;
	private float xmax;
	private float xmin;


	public GameObject[] objects;


	// Use this for initialization
	void Start () {
		float distanceToCamera = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftEdge = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distanceToCamera));
		Vector3 rightEdge = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distanceToCamera));

		xmax = rightEdge.x;
		xmin = leftEdge.x;

		gameLevelLoaded = Application.loadedLevelName;
		if(gameLevelLoaded == "Start") {
			Debug.Log("Loaded level " + Application.loadedLevel);
		}
		if(gameLevelLoaded == "level-01") {
			Debug.Log("Loaded level " + Application.loadedLevel);
			SpawnUntilFull();
//			SpawnShips();
		}
		if(gameLevelLoaded == "level-02") {
			Debug.Log("Loaded level " + Application.loadedLevel);
			SpawnNextEnemiesUntilFull();
		}
		if(gameLevelLoaded == "level-03"
		   || gameLevelLoaded == "level-04"
		   || gameLevelLoaded == "level-05"
		   || gameLevelLoaded == "level-06"
		   || gameLevelLoaded == "level-07") {
			Debug.Log("Loaded level " + Application.loadedLevel);
			SpawnRandomEnemy();
		}
		if(gameLevelLoaded == "level-boss-01") {
			Debug.Log("Loaded level " + Application.loadedLevel);
			SpawnBossUntilFull();
		}
	}

	public void OnDrawGizmos() {
		// draw box around object (the enemy formation object)
		Gizmos.DrawWireCube(transform.position, new Vector3(width, height, 0));
	}
	
	void Update () {
		if(movingRight) {
			// move to the right at game speed
			transform.position += Vector3.right * speed * Time.deltaTime;
		}
		else {
			// move to the left at game speed
			transform.position += Vector3.left * speed * Time.deltaTime;
		}

		float rightEdgeOfFormation = transform.position.x + (0.5f * width);
		float leftEdgeOfFormation = transform.position.x - (0.5f * width);
		if(leftEdgeOfFormation < xmin) {
			movingRight = true;
		}
		else if(rightEdgeOfFormation > xmax) {
			movingRight = false;
		}

		if(AllMembersDead()) {
//			Debug.Log("Empty formation");
//			Debug.Log("Item count " + EnemyDrops.itemDropCount);
			if(EnemyDrops.itemDropCount == 0) {
				Debug.Log("No items left");
				LevelManager levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
				levelManager.LoadNextLevel();
			}
		}
	}

	Transform NextFreePosition() {
		int count = 0;
		foreach(Transform childPositionGameObject in transform) {
			// childCount is either 1 or 0
			// childCount is a child of Position which is a child of Enemy Formation
			count++;
			Debug.Log("Count is " + count);
			Debug.Log("Child Position " + childPositionGameObject.transform.position);
			if(childPositionGameObject.childCount == 0) {
				return childPositionGameObject;
			}
		}
		return null;
	}

	bool AllMembersDead() {
//		Debug.Log("Number of enemy positions " + transform.childCount);
		// loop over each child Game Object within the EnemyFormation Game Object
		foreach(Transform childPositionGameObject in transform) {
//			Debug.Log("Enemy created or not" + childPositionGameObject.childCount);
			// 0 is dead enemy or not created
			// 1 is alive enemy
			if(childPositionGameObject.childCount > 0) {
				return false;
			}
		}
		return true;
	}

//	void SpawnShips() {
//		Debug.Log("Running SpawnShips");
//		foreach(Transform childPositionGameObject in transform) {
//			Debug.Log("Create Enemy");
//			GameObject enemy =  Instantiate(enemyPrefab, transform.position, Quaternion.identity) as GameObject;
//			enemy.transform.SetParent(childPositionGameObject);
//		}
////		Transform freePosition = NextFreePosition();
////		if(freePosition) {
////			GameObject enemy =  Instantiate(enemyPrefab, freePosition.position, Quaternion.identity) as GameObject;
////			enemy.transform.parent = freePosition;
////		}
////		if(NextFreePosition()) {
////			Invoke ("SpawnShips", spawnDelay);
////		}
//	}

	public void SpawnRandomEnemy()
	{
		Debug.Log("In SpawnRandomEnemy");
//		foreach(Transform childPositionGameObject in transform) {
//			Debug.Log("Create Enemy");
//			GameObject enemy =  Instantiate(objects[UnityEngine.Random.Range(0, objects.Length)], transform.position, Quaternion.identity) as GameObject;
//			enemy.transform.SetParent(childPositionGameObject);
//		}
		Transform freePosition = NextFreePosition();
		if(freePosition) {
			GameObject enemy =  Instantiate(objects[UnityEngine.Random.Range(0, objects.Length)], freePosition.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = freePosition;
		}
		if(NextFreePosition()) {
			Invoke ("SpawnRandomEnemy", spawnDelay);
		}
	}

	void SpawnUntilFull() {
//		foreach(Transform childPositionGameObject in transform) {
//			Debug.Log("Create Enemy");
//			GameObject enemy =  Instantiate(enemyPrefab, transform.position, Quaternion.identity) as GameObject;
//			enemy.transform.SetParent(childPositionGameObject);
//		}
		Transform freePosition = NextFreePosition();
		if(freePosition) {
			GameObject enemy =  Instantiate(enemyPrefab, freePosition.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = freePosition;
		}
		if(NextFreePosition()) {
			Invoke ("SpawnUntilFull", spawnDelay);
		}
	}

	void SpawnNextEnemiesUntilFull() {
//		foreach(Transform childPositionGameObject in transform) {
//			Debug.Log("Create Enemy");
//			GameObject enemy =  Instantiate(enemyPrefab2, transform.position, Quaternion.identity) as GameObject;
//			enemy.transform.SetParent(childPositionGameObject);
//		}
		Transform freePosition = NextFreePosition();
		if(freePosition) {
			GameObject enemy =  Instantiate(enemyPrefab2, freePosition.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = freePosition;
		}
		if(NextFreePosition()) {
			Invoke ("SpawnNextEnemiesUntilFull", spawnDelay);
		}
	}

	void SpawnBossUntilFull() {
//		foreach(Transform childPositionGameObject in transform) {
//			Debug.Log("Create Enemy");
//			GameObject enemy =  Instantiate(boss01, transform.position, Quaternion.identity) as GameObject;
//			enemy.transform.SetParent(childPositionGameObject);
//		}
		Transform freePosition = NextFreePosition();
		if(freePosition) {
			GameObject enemy =  Instantiate(boss01, freePosition.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = freePosition;
		}
		if(NextFreePosition()) {
			Invoke ("SpawnBossUntilFull", spawnDelay);
		}
	}

}
