﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class EnemyBehavior : MonoBehaviour {

	public GameObject[] weapon;
	public GameObject coin;
	public GameObject powerUpWeapon;

//	public GameObject[] enemyDrops;

	public float health = 150.0f;
	public float weaponSpeed = 10.0f;
	public float shotsPerSecond = 0.5f;

	public int scoreValue = 150;

	public static int scoreEnemy;
//	public static int itemDropCount;

	public AudioClip fireSound;
	public AudioClip dieSound;

	private ScoreKeeper scoreKeeper;
	private EnemyDrops enemyDropsLookup;

	void Start() {
		Debug.Log("Finding score and getting the ScoreKeeper component");
		scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKeeper>();
		Debug.Log("Score and component found");

		Debug.Log("Finding EnemyDrops and getting the EnemyDrops component");
		enemyDropsLookup = GameObject.Find("EnemyDrops").GetComponent<EnemyDrops>();
		Debug.Log("EnemyDrops and component found");
	}

	void Update() {
		float probability = Time.deltaTime * shotsPerSecond;
		if(Random.value < probability) {
			Fire();
		}
	}

	void Fire() {
		if(weapon.Length >= 2) {
//			enemyDrops[Random.Range(0, enemyDrops.Length)]
			Debug.Log("Firing multiple weapons");
			for(int i = 0; i < weapon.Length; i++) {
				Debug.Log("Creating weapon number " + i);
				GameObject missile = Instantiate(weapon[i], (transform.position + new Vector3(i * .2f, 0, 0)), Quaternion.identity) as GameObject;
				missile.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -weaponSpeed, 0);
				Debug.Log("missle is " + missile);
			}
		}
		else {
			GameObject missile = Instantiate(weapon[Random.Range(0, weapon.Length)], transform.position, Quaternion.identity) as GameObject;
			missile.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -weaponSpeed, 0);
		}
		AudioSource.PlayClipAtPoint(fireSound, transform.position);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		// anything that collides with the collider, grab that game object of that collider
		// then grab the Weapon component of that game object
		Weapon weapon = collider.gameObject.GetComponent<Weapon>();
		if(weapon) {
			Debug.Log("Hit by a player weapon");
			Debug.Log("Enemy health is at " + health);
			StartCoroutine(Flasher());
			health -= weapon.GetDamage();
			Debug.Log("Enemy health is now at " + health);
			weapon.Hit();
			if(health <= 0) {
				Debug.Log("Enemy health is zero or below. dying. " + health);
				Die();
			}
		}
	}

	IEnumerator Flasher() 
	{
		Debug.Log("Running Flasher");
		for (int i = 0; i < 3; i++)
		{
			Debug.Log("Flash");
			GetComponent<Renderer>().material.color = Color.magenta;
			yield return new WaitForSeconds(.05f);
			GetComponent<Renderer>().material.color = Color.cyan;
			yield return new WaitForSeconds(.05f);
			GetComponent<Renderer>().material.color = Color.yellow;
			yield return new WaitForSeconds(.05f);
			GetComponent<Renderer>().material.color = Color.white;
			yield return new WaitForSeconds(.05f);
		}
	}

	public void ScoreEnemy(int points) {
		Debug.Log("Score enemy");
		Debug.Log("Adding " + points + " to score enemy");
		scoreEnemy += points;
		Debug.Log("Total points from Enemies is " + scoreEnemy);
	}

//	public void CreateItem() {
//		Debug.Log("Running CreateItem");
//		Instantiate(enemyDrops[Random.Range(0, enemyDrops.Length)], transform.position, Quaternion.identity);
//		Debug.Log("Items floating around " + itemDropCount);
//		itemDropCount++;
//		Debug.Log("Items floating around " + itemDropCount);
//	}

	void Die() {
		Debug.Log("Enemy died");
		AudioSource.PlayClipAtPoint(dieSound, transform.position, 0.1f);
		Debug.Log("Enemy being destroyed is " + gameObject);
		Destroy(gameObject);
//		CreateItem();
		enemyDropsLookup.DropLookup(gameObject);
		Debug.Log("Updating Score value " + scoreValue);
		scoreKeeper.Score(scoreValue);
		ScoreEnemy(scoreValue);
	}
}
