﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed = 15.0f;
	public float padding = 1.0f;
	public GameObject laser;
	public GameObject coin;
	private GameObject powerUpWeapon;
	private GameObject powerUpShield;
	private GameObject powerUpHealth;
	private GameObject shieldPrefabFull;
	private GameObject shieldPrefabHalf;
	private GameObject shieldPrefabQuarter;

	public float weaponSpeed = 5.0f;
	public float firingRate = 0.2f;

	public AudioClip fireSound;

	private PlayerHealth playerHealth;
	private PlayerShield playerShield;

	float xmin;
	float xmax;

	void Start() {
		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));

		xmin = leftmost.x + padding;
		xmax = rightmost.x - padding;

		Debug.Log("Finding Health and getting the PlayerHealth component");
		playerHealth = GameObject.Find("Player Health").GetComponent<PlayerHealth>();
		Debug.Log("Player Health and component found");

		Debug.Log("Finding Shield and getting the PlayerShield component");
		playerShield = GameObject.Find("Player Shield").GetComponent<PlayerShield>();
		Debug.Log("Player Shield and component found");

		shieldPrefabFull = GameObject.Find("shieldPowerFull");
		shieldPrefabHalf = GameObject.Find("shieldPowerHalf");
		shieldPrefabQuarter = GameObject.Find("shieldPowerQuarter");
		ShieldCheck();
	}

	void Fire() {
		// base create object
//		Instantiate(weapon, transform.position, Quaternion.identity);

		// modifiable game object
		Vector3 startPosition = transform.position + new Vector3(0, 1, 0);
		GameObject weapon = Instantiate(laser, startPosition, Quaternion.identity) as GameObject;
		weapon.GetComponent<Rigidbody2D>().velocity = new Vector3(0, weaponSpeed, 0);
		AudioSource.PlayClipAtPoint(fireSound, transform.position);
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)) {
			InvokeRepeating("Fire", 0.00001f, firingRate);
		}
		if(Input.GetKeyUp(KeyCode.Space)) {
			CancelInvoke("Fire");
		}

		if(Input.GetKey(KeyCode.LeftArrow)) {
			// move X position at game speed, don't move Y position
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
		else if(Input.GetKey(KeyCode.RightArrow)) {
			// move X position at game speed, don't move Y position
			transform.position += Vector3.right * speed * Time.deltaTime;
		}

		// restrict the player to the gamespace
		float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
		transform.position = new Vector3(newX, transform.position.y, transform.position.z);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		Weapon weapon = collider.gameObject.GetComponent<Weapon>();
		Coin coin = collider.gameObject.GetComponent<Coin>();
		PowerUpWeapon powerUpWeapon = collider.gameObject.GetComponent<PowerUpWeapon>();
		PowerUpShield powerUpShield = collider.gameObject.GetComponent<PowerUpShield>();
		PowerUpHealth powerUpHealth = collider.gameObject.GetComponent<PowerUpHealth>();
		if(weapon) {
			Debug.Log("Player hit by Enemy weapon");
			Debug.Log("Player health is at " + PlayerHealth.playerHealth);
			StartCoroutine(Flasher());
			if(PlayerShield.playerShield > 0) {
				Debug.Log("shield is up");
				Debug.Log("shield is at " + PlayerShield.playerShield);
				playerShield.Shield(weapon.GetDamage());
				Debug.Log("shield is now at " + PlayerShield.playerShield);
				weapon.Hit();
				ShieldCheck();
			}
			else {
				Debug.Log("Shield is down");
				playerHealth.Health(weapon.GetDamage());
				Debug.Log("Player health is now at " + PlayerHealth.playerHealth);
				weapon.Hit();
				if(PlayerHealth.playerHealth <= 0) {
					Debug.Log("Player health is zero or below. dying. " + PlayerHealth.playerHealth);
					Die();
				}
			}
		}
		if(coin) {
			Debug.Log("Grabbed coin");
			coin.Collect();
		}
		if(powerUpWeapon) {
			Debug.Log("Grabbed PowerUp Weapon");
			powerUpWeapon.Collect();
		}
		if(powerUpHealth) {
			Debug.Log("Grabbed PowerUp Health");
			Debug.Log("Player health is at " + PlayerHealth.playerHealth);
			playerHealth.HealthRestore(powerUpHealth.GetHealthRestore());
			Debug.Log("Player health is now at " + PlayerHealth.playerHealth);
			powerUpHealth.Collect();
		}
		if(powerUpShield) {
			Debug.Log("Grabbed PowerUp Shield");
			playerShield.ShieldRestore(powerUpShield.GetShieldRestore());
			powerUpShield.Collect();
			ShieldCheck();
		}
	}

	void ShieldCheck() {
		Debug.Log("Shield Check");
		if(PlayerShield.playerShield > 250) {
			Debug.Log("Shield is above half");
			shieldPrefabFull.SetActive(true);
			shieldPrefabHalf.SetActive(false);
			shieldPrefabQuarter.SetActive(false);
		}
		else if(PlayerShield.playerShield > 125) {
			Debug.Log("Shield is between half and a quarter");
			shieldPrefabFull.SetActive(false);
			shieldPrefabHalf.SetActive(true);
			shieldPrefabQuarter.SetActive(false);
		}
		else if(PlayerShield.playerShield > 0) {
			Debug.Log("Shield is under a quarter");
			shieldPrefabFull.SetActive(false);
			shieldPrefabHalf.SetActive(false);
			shieldPrefabQuarter.SetActive(true);
		}
		else {
			Debug.Log("Shield should be down");
			shieldPrefabFull.SetActive(false);
			shieldPrefabHalf.SetActive(false);
			shieldPrefabQuarter.SetActive(false);
		}
	}

	IEnumerator Flasher() 
	{
		Debug.Log("Running Flasher");
		for (int i = 0; i < 3; i++)
		{
			float flashTimer = .05f;
			Debug.Log("Flash");
			GetComponent<Renderer>().material.color = Color.magenta;
			yield return new WaitForSeconds(flashTimer);
			GetComponent<Renderer>().material.color = Color.cyan;
			yield return new WaitForSeconds(flashTimer);
			GetComponent<Renderer>().material.color = Color.yellow;
			yield return new WaitForSeconds(flashTimer);
			GetComponent<Renderer>().material.color = Color.white;
			yield return new WaitForSeconds(flashTimer);
		}
	}

	void Die() {
		Debug.Log("Player Died");
		Destroy(gameObject);
		LevelManager levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
		levelManager.LoadLevel("Lose");
	}
}
