﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

	public int coinValue;
	public static int scoreCoin;

	private ScoreKeeper scoreKeeper;

	// Use this for initialization
	void Start () {
		Debug.Log("Coin created");
		Debug.Log("Finding score and getting the ScoreKeeper component");
		scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKeeper>();
		Debug.Log("Score and component found");
	}

	public void ScoreCoin(int points) {
		Debug.Log("Score coin");
		Debug.Log("Adding " + points + " to score coin");
		scoreCoin += points;
		Debug.Log("Total points from Coins is " + scoreCoin);
	}
	
	public void Collect() {
		Debug.Log("Coin collected. Destroying coin.");
		Debug.Log("Updating Score value " + coinValue);
		scoreKeeper.Score(coinValue);
		ScoreCoin(coinValue);
		Destroy(gameObject);
		Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
		EnemyDrops.itemDropCount--;
		Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
	}
}
