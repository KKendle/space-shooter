﻿using UnityEngine;
using System.Collections;

public class EnemyDrops : MonoBehaviour {

	public static int itemDropCount;
	public GameObject[] enemyDrops;

	public void DropLookup(GameObject enemyKilled) {
		Debug.Log("Running DropLookUp");
		DropChance(enemyKilled);
	}

	void DropChance(GameObject enemyKilled) {
		Debug.Log("Running DropChance");
		Debug.Log("The enemy killed is " + enemyKilled);
		string enemyName = enemyKilled.ToString();
		Debug.Log("Enemy Name is " + enemyName);

		float dropChance = Random.Range(0.0f, 100.0f);
		Debug.Log("Item Drop Chance of " + dropChance);

		if(enemyName == "Enemy01(Clone) (UnityEngine.GameObject)") {
			Debug.Log("Killed a type01 enemy");
//			Instantiate(enemyDrops[2], enemyKilled.transform.position, Quaternion.identity);
//			Debug.Log("Items floating around " + itemDropCount);
//			itemDropCount++;
//			Debug.Log("Items floating around " + itemDropCount);
			if(dropChance <= 20.0f) {
				Debug.Log("Dropping Item");
				CreateItem(enemyKilled);
			}
			else {
				Debug.Log("Not dropping an item");
			}
		}
		else if(enemyName == "Enemy02(Clone) (UnityEngine.GameObject)") {
			Debug.Log("Killed a type02 enemy");
			if(dropChance <= 25.0f) {
				Debug.Log("Dropping Item");
				CreateItem(enemyKilled);
			}
			else {
				Debug.Log("Not dropping an item");
			}
		}
		else if(enemyName == "Enemy03(Clone) (UnityEngine.GameObject)") {
			Debug.Log("Killed a type03 enemy");
			if(dropChance <= 35.0f) {
				Debug.Log("Dropping Item");
				CreateItem(enemyKilled);
			}
			else {
				Debug.Log("Not dropping an item");
			}
		}
		else if(enemyName == "Enemy04(Clone) (UnityEngine.GameObject)") {
			Debug.Log("Killed a type04 enemy");
			if(dropChance <= 45.0f) {
				Debug.Log("Dropping Item");
				CreateItem(enemyKilled);
			}
			else {
				Debug.Log("Not dropping an item");
			}
		}
		else if(enemyName == "Enemy05(Clone) (UnityEngine.GameObject)") {
			Debug.Log("Killed a type05 enemy");
			if(dropChance <= 60.0f) {
				Debug.Log("Dropping Item");
				CreateItem(enemyKilled);
			}
			else {
				Debug.Log("Not dropping an item");
			}
		}
		else if(enemyName == "Enemy06(Clone) (UnityEngine.GameObject)") {
			Debug.Log("Killed a type06 enemy");
			if(dropChance <= 75.0f) {
				Debug.Log("Dropping Item");
				CreateItem(enemyKilled);
			}
			else {
				Debug.Log("Not dropping an item");
			}
		}
		else if(enemyName == "Enemy07(Clone) (UnityEngine.GameObject)") {
			Debug.Log("Killed a type07 enemy");
			if(dropChance <= 90.0f) {
				Debug.Log("Dropping Item");
				CreateItem(enemyKilled);
			}
			else {
				Debug.Log("Not dropping an item");
			}
		}
		else if(enemyName == "Boss01(Clone) (UnityEngine.GameObject)") {
			Debug.Log("Killed a type01 boss");
			// drop chance of 100%
			// drop multiple items
			CreateItem(enemyKilled);
			CreateItem(enemyKilled);
		}
		else {
			Debug.Log("Unknown Enemy");
		}
	}

	void CreateItem(GameObject enemyKilled) {
		Debug.Log("Running CreateItem");

		Instantiate(enemyDrops[Random.Range(0, enemyDrops.Length)], enemyKilled.transform.position, Quaternion.identity);
//		Instantiate(enemyDrops[itemDrop], enemyKilled.transform.position, Quaternion.identity);
		Debug.Log("Items floating around " + itemDropCount);
		itemDropCount++;
		Debug.Log("Items floating around " + itemDropCount);
	}
}
