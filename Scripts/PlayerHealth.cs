﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

	// static means it belongs to class itself, not a created instance of the class
	// meaning, there is only one playerHealth
	public static int playerHealth = 1000;
	static PlayerHealth instance = null;

	private Text healthText;

	void Start() {
		if (instance != null && instance != this) {
			Destroy(gameObject);
		}
		else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			healthText = GetComponent<Text>();
			healthText.text = playerHealth.ToString();
		}
	}

	public void Health(int points) {
		playerHealth -= points;
		healthText.text = playerHealth.ToString();
	}

	public void HealthRestore(int points) {
		playerHealth += points;
		healthText.text = playerHealth.ToString();
	}

	public static void Reset() {
		Debug.Log("Player health is at " + playerHealth);
		Debug.Log("Resetting player health");
		playerHealth = 1000;
		Debug.Log("Player health now is at " + playerHealth);
	}
}
