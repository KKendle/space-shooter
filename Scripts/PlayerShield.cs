﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerShield : MonoBehaviour {

	public static int playerShield = 500;
	static PlayerShield instance = null;

	private Text shieldText;

	void Start() {
		if (instance != null && instance != this) {
			Destroy(gameObject);
		}
		else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			shieldText = GetComponent<Text>();
			shieldText.text = playerShield.ToString();
		}
	}

	public void Shield(int points) {
		playerShield -= points;
		shieldText.text = playerShield.ToString();
		if(playerShield <= 0) {
			playerShield = 0;
			shieldText.text = playerShield.ToString();
		}
	}

	public void ShieldRestore(int points) {
		playerShield += points;
		shieldText.text = playerShield.ToString();
	}

	public static void Reset() {
		Debug.Log("Player shield is at " + playerShield);
		Debug.Log("Resetting player shield");
		playerShield = 500;
		Debug.Log("Player shield now is at " + playerShield);
	}
}
