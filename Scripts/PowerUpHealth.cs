﻿using UnityEngine;
using System.Collections;

public class PowerUpHealth : MonoBehaviour {

	public int healthRestore = 100;
	
	public int GetHealthRestore() {
		Debug.Log("Health Restored " + healthRestore);
		return healthRestore;
	}

	void Start () {
		Debug.Log("PowerUp Health created");
	}

	public void Collect() {
		Debug.Log("PowerUp Health collected. Destroying PowerUp Health.");
		Destroy(gameObject);
		Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
		EnemyDrops.itemDropCount--;
		Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
	}
}
