﻿using UnityEngine;
using System.Collections;

public class PowerUpShield : MonoBehaviour {

	public int shieldRestore = 100;

	void Start() {
		Debug.Log("PowerUp Shield created");
	}

	public int GetShieldRestore() {
		Debug.Log("Shield Restored " + shieldRestore);
		return shieldRestore;
	}
	
	public void Collect() {
		Debug.Log("PowerUp Shield collected. Destroying PowerUp Shield.");
		Destroy(gameObject);
		Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
		EnemyDrops.itemDropCount--;
		Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
	}
}
