﻿using UnityEngine;
using System.Collections;

public class PowerUpWeapon : MonoBehaviour {

	public float weaponDamageMultiplier = 1.25f;

	// Use this for initialization
	void Start () {
		Debug.Log("PowerUp Weapon created");
	}

	public float GetMultiplier() {
		Debug.Log("Getting Weapon Damage Multiplier");
		Debug.Log("Damage done " + weaponDamageMultiplier);
		return weaponDamageMultiplier;
	}
	
	public void Collect() {
		Debug.Log("PowerUp Weapon collected. Destroying PowerUp Weapon.");
		Destroy(gameObject);
		Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
		EnemyDrops.itemDropCount--;
		Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
	}
}
