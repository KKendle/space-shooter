﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreDisplay : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Debug.Log("Find Score");
		Text scoreText = GameObject.Find("ScoreTotal").GetComponent<Text>();
		scoreText.text = ScoreKeeper.score.ToString();
		Debug.Log("Should have found score");

//		Debug.Log("Find HighScore");
//		Text highScoreText = GameObject.Find("HighScoreTotal").GetComponent<Text>();
//		highScoreText.text = ScoreKeeper.highScore.ToString();
//		Debug.Log("Should have found highscore");

//		PlayerPrefs.Save();
//		int getHighscore = PlayerPrefs.GetInt("highscore");
//		Debug.Log("Get high score is " + getHighscore);

		ResetGameStats();
	}

	void ResetGameStats() {
		ScoreKeeper.Reset();
		PlayerHealth.Reset();
		PlayerShield.Reset();
		EnemyDrops.itemDropCount = 0;
		EnemyBehavior.scoreEnemy = 0;
		Coin.scoreCoin = 0;
	}

}
