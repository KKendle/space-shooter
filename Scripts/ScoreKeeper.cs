﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	// static means it belongs to class itself, not a created instance of the class
	// meaning, there is only one score
	public static int score;
	public static int highScore;

	static ScoreKeeper instance = null;

	private Text scoreText;

	void Start() {
		if (instance != null && instance != this) {
			Destroy(gameObject);
		}
		else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			scoreText = GetComponent<Text>();
			scoreText.text = score.ToString();
		}
	}

	public void Score(int points) {
		Debug.Log("Running Score");
		score += points;
		scoreText.text = score.ToString();
		Debug.Log("Score is " + score.ToString());
//		HighScore();
//		StoreHighscore(score);
	}

//	public void HighScore() {
//		Debug.Log("Running High score");
//		Debug.Log("High score is " + highScore.ToString());
////		highScore = PlayerPrefs.GetInt("highscore");
//		Debug.Log("Getting score of " + highScore);
//
//		if(score > highScore) {
//			Debug.Log("New highscore reached");
//			highScore = score;
////			PlayerPrefs.SetInt("highscore", highScore);
//			Debug.Log("The new HighScore is " + highScore.ToString());
//		}
//		else {
//			Debug.Log("No new highscore");
//		}
//	}

//	void StoreHighscore(int newHighscore)
//	{
//		Debug.Log("Running StoreHighscore");
//		int oldHighscore = PlayerPrefs.GetInt("highscore");    
//		if(newHighscore > oldHighscore) {
//			PlayerPrefs.SetInt("highscore", newHighscore);
//			PlayerPrefs.Save();
//			Debug.Log("Looking at the old score" + oldHighscore);
//		}
//		Debug.Log("Looking at the old score" + oldHighscore);
//	}

	public static void Reset() {
		score = 0;
	}
}
