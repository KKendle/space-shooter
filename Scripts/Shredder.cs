﻿using UnityEngine;
using System.Collections;

public class Shredder : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D collider) {
		Coin coin = collider.gameObject.GetComponent<Coin>();
		PowerUpWeapon powerUpWeapon = collider.gameObject.GetComponent<PowerUpWeapon>();
		PowerUpShield powerUpShield = collider.gameObject.GetComponent<PowerUpShield>();
		PowerUpHealth powerUpHealth = collider.gameObject.GetComponent<PowerUpHealth>();

		Destroy(collider.gameObject);

		if(coin || powerUpWeapon || powerUpHealth || powerUpShield) {
			Debug.Log("Shredding " + collider.gameObject);
			Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
			EnemyDrops.itemDropCount--;
			Debug.Log("Items floating around " + EnemyDrops.itemDropCount);
		}
	}
}
