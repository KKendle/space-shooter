﻿using UnityEngine;
using System.Collections;

public class Starfield : MonoBehaviour {

	static Starfield instance = null;
	
	void Start () {
		if (instance != null && instance != this) {
			Destroy(gameObject);
			Debug.Log("Duplicate Starfield found. Destroying");
		}
		else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			Debug.Log("First Starfield created");
		}
	}
}
