﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public int damage = 100;

	public int GetDamage() {
		Debug.Log("Damage done " + damage);
		return damage;
	}

	public void Hit() {
		Debug.Log("Weapon Hit. Destroying weapon.");
		Destroy(gameObject);
	}
}
